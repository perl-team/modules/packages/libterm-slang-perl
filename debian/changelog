libterm-slang-perl (0.07-15) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ gregor herrmann ]
  * debian/watch: use uscan macros.

  [ Étienne Mollier ]
  * gcc-14.patch: new: fix incompatible pointer type assignment.
    (Closes: #1075211)
  * remaining-changes.patch: unfuzz.
  * Declare compliance with Debian Policy 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Sun, 21 Jul 2024 19:05:58 +0200

libterm-slang-perl (0.07-14) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Drop debian/README.source which talked about quilt.
  * Rename debian/docs to debian/libterm-slang-perl.docs for consistency
    with most other packages.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Drop override_dh_compress.
    debhelper in compat level 12 doesn't compress examples anymore.
  * Add debian/tests/pkg-perl/use-name for autopkgtests.

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:48:23 +0100

libterm-slang-perl (0.07-13) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Axel Beckert ]
  * Remove trailing whitespace from ancient debian/changelog entry.
  * Declare compliance with Debian Policy 4.1.1.
  * Enable all hardening build flags (except format as before).
  * Mark package as autopkgtestable.
  * Convert debian/copyright to machine-readable DEP5 format.
  * Set "Rules-Requires-Root: no".
  * Bump debhelper compatibility level to 10.
    + Update versioned debhelper build-dependency accordingly.
  * Apply "wrap-and-sort -a".

 -- Axel Beckert <abe@debian.org>  Thu, 16 Nov 2017 02:29:55 +0100

libterm-slang-perl (0.07-12) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Use dist-based URL in debian/watch.
  * Set debhelper compatibility level to 5.
  * debian/rules:
    - don't install empty /usr/share/perl5 directory
    - remove unneeded call to dh_link
  * debian/rules: delete /usr/share/perl5 only if it exists.
  * Revert some changes to upstream code that were caused by an accidentally
    set svn:keywords property.
  * Split remaining changes out into slang2.patch; add quilt framework.
  * Set Standards-Version to 3.7.3 (no changes).
  * Remove build dependency on historic dpkg-dev version.
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Joey Hess ]
  * Removed myself from uploaders field

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * Switch to source format "3.0 (quilt)"
    + Remove quilt traces from debian/rules and quilt build-dependency
    + Put remaining not-yet-quiltified patches into their own patch
  * Fix pkg-perl lintian warning arch-any-package-needs-newer-debhelper
  * Add copyright years to debian/copyright to fix lintian warning
    copyright-without-copyright-notice. Also add upstream's last name.
  * Fine-tune short and long description a little bit.
  * Bump debhelper compatibility to 9
  * Revamp debian/rules
    + Fix lintian warning debian-rules-missing-recommended-target
    + Replace "dh_clean -k" with "dh_prep"
    + Remove obsolete dh_{clean,installchangelogs} parameters
    + Move dh_install{docs,examples} parameters to debian/{docs,examples}
    + Use dh_auto_{configure,build,test,install,clean}
      - Disable "format" hardening for now, c.f. #661546
      - Fixes lintian warning hardening-no-fortify-functions
    + Remove obsolete /usr/share/perl5 handling
    + Remove obsolete variable usage
    + Finally switch to a minimal dh-style debian/rules file
  * Bump Standards-Version to 3.9.5 (no further changes)
  * Do not compress example scripts
  * Add Uploader header again and list myself as uploader

 -- Axel Beckert <abe@debian.org>  Tue, 24 Dec 2013 02:54:49 +0100

libterm-slang-perl (0.07-11) unstable; urgency=low

  * Now maintained by the Debian perl group.
  * Updated to current policy.
  * Minor fixes to debian/rules.

 -- Joey Hess <joeyh@debian.org>  Mon, 30 Jul 2007 01:36:54 -0400

libterm-slang-perl (0.07-10) unstable; urgency=low

  * Updated to slang 2. Only type and prototype changes, did not add new
    functions. Did not port to new readline interface, so all readline support
    is currently disabled.

 -- Joey Hess <joeyh@debian.org>  Mon, 20 Jun 2005 15:06:44 -0400

libterm-slang-perl (0.07-9) unstable; urgency=low

  * Build for perl 5.8.

 -- Joey Hess <joeyh@debian.org>  Wed, 31 Jul 2002 03:06:07 +0000

libterm-slang-perl (0.07-8) unstable; urgency=low

  * debhelper v4

 -- Joey Hess <joeyh@debian.org>  Mon, 29 Jul 2002 19:04:07 +0000

libterm-slang-perl (0.07-7) unstable; urgency=low

  * Removed unnecessary README, Closes: #118220

 -- Joey Hess <joeyh@debian.org>  Mon,  5 Nov 2001 16:29:26 -0500

libterm-slang-perl (0.07-6) unstable; urgency=low

  * Updated to new perl policy.
  * Updated to debhelper v3.

 -- Joey Hess <joeyh@debian.org>  Thu, 15 Feb 2001 18:00:24 -0800

libterm-slang-perl (0.07-5) unstable; urgency=low

  * Gar! Closes: #84219

 -- Joey Hess <joeyh@debian.org>  Tue, 30 Jan 2001 23:23:18 -0800

libterm-slang-perl (0.07-4) unstable; urgency=low

  * Added build depends, Closes: #84113

 -- Joey Hess <joeyh@debian.org>  Tue, 30 Jan 2001 11:15:15 -0800

libterm-slang-perl (0.07-3) unstable; urgency=low

  * Rebuilt with perl 5.6. I wouldn't normally bother, since the old
    packages work with perl 5.6, but the dpeendancies got messed up a while
    ago while I was using bod's perl 5.6 debs and an old debhelper.

 -- Joey Hess <joeyh@debian.org>  Tue,  7 Nov 2000 16:00:54 -0800

libterm-slang-perl (0.07-2) unstable; urgency=low

  * Build in binary-arch target, not binary-indep. Closes: #63941

 -- Joey Hess <joeyh@debian.org>  Thu, 11 May 2000 13:48:43 -0700

libterm-slang-perl (0.07-1) unstable; urgency=low

  * New upstream, incorporating some of my changes.

 -- Joey Hess <joeyh@debian.org>  Sun, 16 Apr 2000 20:45:22 -0700

libterm-slang-perl (0.06-3) unstable; urgency=low

  * Added SLtt_set_screen_size function. No analog exists in slang, but
    this lets you set SLtt_Screen_Rows and SLtt_Screen_Cols from inside
    perl. Passed upstream; this interface may change w/o warning.

 -- Joey Hess <joeyh@debian.org>  Thu,  6 Apr 2000 19:48:51 -0700

libterm-slang-perl (0.06-2) unstable; urgency=low

  * Made SLsmg_set_screen_start take normal integers, and convert to int
    pointers in the XS code. I'll toss this upstream, the function doesn't
    seem usable in perl w/o this change.

 -- Joey Hess <joeyh@debian.org>  Thu,  6 Apr 2000 14:09:47 -0700

libterm-slang-perl (0.06-1) unstable; urgency=low

  * New upstream, incorporating all my changes.

 -- Joey Hess <joeyh@debian.org>  Wed, 22 Mar 2000 12:18:40 -0800

libterm-slang-perl (0.05-3) unstable; urgency=low

  * Removed prototype, thus fixing all the nasty prototype mismatch
    warnings when accessing constants.

 -- Joey Hess <joeyh@debian.org>  Mon, 20 Mar 2000 13:15:46 -0800

libterm-slang-perl (0.05-2) unstable; urgency=low

  * Added SLtt_set_mono to export list.
  * Re-added support for SLsmg_reinit_smg.

 -- Joey Hess <joeyh@debian.org>  Mon, 20 Mar 2000 12:31:26 -0800

libterm-slang-perl (0.05-1) unstable; urgency=low

  * Major new upsteam release, with a completly different API.
  * Reintegrated my patch for SLtt_set_mono.

 -- Joey Hess <joeyh@debian.org>  Mon, 20 Mar 2000 11:39:30 -0800

libterm-slang-perl (0.01-4) unstable; urgency=low

  * Added support for the SLtt_set_mono function. This includes constants
    for *_MASK.

 -- Joey Hess <joeyh@debian.org>  Fri, 17 Mar 2000 19:07:19 -0800

libterm-slang-perl (0.01-3) unstable; urgency=low

  * Added support for the SLsmg_reinit_smg() function.

 -- Joey Hess <joeyh@debian.org>  Fri, 17 Mar 2000 16:58:01 -0800

libterm-slang-perl (0.01-2) unstable; urgency=low

  * Thanks to Brendan O'Dea <bod@compusol.com.au>, all the slang constants
    are now included in the XS file. This closes an upstream TODO item.
    Access them like $Term::Slang::KEY_BACKSPACE
  * I have sent diffs upstream, but until they are accepted, this is a
    debian-specific feature.

 -- Joey Hess <joeyh@debian.org>  Sat, 11 Mar 2000 21:02:30 -0800

libterm-slang-perl (0.01-1) unstable; urgency=low

  * First release.
  * Minor changes to pod docs to make SYNOPSIS be indented correctly.

 -- Joey Hess <joeyh@debian.org>  Wed,  8 Mar 2000 20:16:38 -0800
